package com.springboot.lightrpc;

import org.junit.Test;

public class TestServer {
    @Test
    public void testStart() throws Exception {
        LightRpcServer lightRpcServer = new LightRpcServer();
        lightRpcServer.start(8088);
    }
}
