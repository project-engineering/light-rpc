package com.springboot.lightrpc;

import com.springboot.lightrpc.handler.LightRpcHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

public class LightRpcChannelInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel channel) throws Exception {
        //把handler组成一个管道
        channel.pipeline().addLast(new LightRpcHandler());
    }
}
