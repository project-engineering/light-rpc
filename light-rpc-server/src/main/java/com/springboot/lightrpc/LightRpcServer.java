package com.springboot.lightrpc;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.extern.log4j.Log4j2;

/**
 * Light-Rpc服务端
 */
@Log4j2
public class LightRpcServer {
    /**
     * 服务启动
     */
    public void start(int port) throws Exception{
        //主线程，负责客户端的连接的建立，它不处理业务逻辑
        EventLoopGroup boss = new NioEventLoopGroup(1);

        //工作线程，默认的线程数为：cpu核数 * 2
        EventLoopGroup worker = new NioEventLoopGroup();
        try {
            //服务启动辅助对象
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            //设置线程组
            serverBootstrap.group(boss,worker);
            //配置server的通道类型
            serverBootstrap.channel(NioServerSocketChannel.class)
                    //worker处理器
                    .childHandler(new LightRpcChannelInitializer());
            //绑定端口，启动服务
            ChannelFuture future = serverBootstrap.bind(port).sync();
            log.info("Light Rpc服务器启动成功，端口为："+port);
            //等待服务关闭
            future.channel().closeFuture().sync();
        } finally {
            //优雅的关闭
            boss.shutdownGracefully();
            worker.shutdownGracefully();
        }
    }
}
