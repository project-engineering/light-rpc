package com.springboot.lightrpc;

import com.springboot.lightrpc.handler.LightRpcClientHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class LightRpcClient {
    /**
     * 客户端启动
     */
    public void start(String host,int port) throws Exception{
        //工作线程，默认的线程数为：cpu核数 * 2
        EventLoopGroup worker = new NioEventLoopGroup();
        try {
            //服务启动辅助对象
            Bootstrap bootstrap = new Bootstrap();
            //设置线程组
            bootstrap.group(worker);
            //配置通道类型
            bootstrap.channel(NioSocketChannel.class)
                    //worker处理器
                    .handler(new LightRpcClientHandler());
            //连接服务端
            ChannelFuture future = bootstrap.connect(host,port);
            log.info("连接服务端成功，端口为："+port);
            //等待服务关闭
            future.channel().closeFuture().sync();
        } finally {
            //优雅的关闭
            worker.shutdownGracefully();
        }
    }
}
